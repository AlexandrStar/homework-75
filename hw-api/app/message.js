const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const router = express.Router();


router.post('/encode', (req, res) => {
  console.log(req.body);
  if (!req.body.password)res.status(500).send('password not detected');
  const coded = Vigenere.Cipher(req.body.password.replace(' ','')).crypt(req.body.message);
  res.send(coded)
});

router.post('/decode',(req,res) => {
  if (!req.body.password)res.status(500).send('password not detected');
  const decoded = Vigenere.Decipher(req.body.password.replace(' ','')).crypt(req.body.message);
  res.send(decoded)
});

module.exports = router;