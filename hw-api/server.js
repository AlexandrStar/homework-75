const express = require('express');
const message = require('./app/message');

const cors = require('cors');

const app = express();


app.use(express.json());
app.use(cors());

const port = 8000;

app.use('/message', message);

app.listen(port, () => {
  console.log(`Server started on ${port} port!`);
});