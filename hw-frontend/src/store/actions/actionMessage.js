import axios from '../../axios-api';

export const CODED_MESSAGE = 'CODED_MESSAGE';

export const inputHandler = text => {return {type: CODED_MESSAGE, text}};

export const decodedMessage = messageData => {
  return dispatch => {
    return axios.post('/decode', messageData).then(
      response => dispatch(inputHandler({type: 'encode', value: response.data}))
    );
  };
};

export const encodedMessage = messageData => {
  return dispatch => {
    return axios.post('/encode', messageData).then(
      response => dispatch(inputHandler({type: 'decode', value: response.data}))
    );
  };
};

