import {CODED_MESSAGE} from "../actions/actionMessage";

const initialState = {
  decode: '',
  encode: '',
  password: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CODED_MESSAGE:
      return {...state, [action.text.type]: action.text.value};
    default:
      return state;
  }
};

export default reducer;