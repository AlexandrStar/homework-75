import React, {Component} from 'react';
import './App.css';
import {Button, Col, FormGroup, Input, Label} from "reactstrap";
import {decodedMessage, encodedMessage, inputHandler} from "./store/actions/actionMessage";
import {connect} from "react-redux";

class App extends Component {
  inputChangeHandler = event => {
    const data = {
      type: event.target.name,
      value: event.target.value,
    };
    this.props.inputHandler(data);
  };

  encodeMessage = () => {
    const data = {
      message: this.props.state.encode,
      password: this.props.state.password,
    };
    this.props.encodedMessage(data);
  };

  decodeMessage = () => {
    const data = {
      message: this.props.state.decode,
      password: this.props.state.password,
    };
    this.props.decodedMessage(data);
  };

  render() {
    return (
      <div className="App">
        <FormGroup row>
          <Label sm={2} for="decode">Decoded message</Label>
          <Col sm={5}>
            <Input
              type="textarea" required
              name="decode" id="decode"
              placeholder="Enter message"
              value={this.props.state.decode}
              onChange={(event) => this.inputChangeHandler(event)}
            />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label sm={2} for="pass">Password</Label>
          <Col sm={2}>
            <Input
              type="text" required
              name="password" id="pass"
              placeholder="Enter password"
              value={this.props.state.password}
              onChange={(event) => this.inputChangeHandler(event)}
            />
          </Col>
          <Col sm={2}>
            <Button
              onClick={this.encodeMessage}
              style={{marginRight: "20px"}}
              type="submit"
              color="primary"
            >
              /\
            </Button>
            <Button
              onClick={this.decodeMessage}
              type="submit"
              color="primary"
            >
              \/
            </Button>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label sm={2} for="encode">Encoded message</Label>
          <Col sm={5}>
            <Input
              type="textarea" required
              name="encode" id="encode"
              placeholder="Enter message"
              value={this.props.state.encode}
              onChange={(event) => this.inputChangeHandler(event)}
            />
          </Col>
        </FormGroup>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  state: state.decode,
});

const mapDispatchToProps = dispatch => ({
  inputHandler: text => dispatch(inputHandler(text)),
  decodedMessage: decode => dispatch(decodedMessage(decode)),
  encodedMessage: encode => dispatch(encodedMessage(encode))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
